# Django-Prettyfier

Django-Prettyfier is a django middleware which enhance typography according to french rules.

## Installation and Usage 

### Installation

    git submodule add bitbucket.org:xael/django_prettyfier.git

### Usage

Simply add it to the end of your MIDDLEWARE_CLASSES list:

    MIDDLEWARE_CLASSES += [
        'django_prettyfier.middleware.prettyfier.PrettyText',
    ]


## What it does

Django-Prettyfier pass text thought typogrify and modify text to respect some
rules for french typography such as :

* [space],
* [space]:[space]
* [space];[space]
* .[space]
* [space]!
* [space]?


# -*- coding: utf-8 -*-

from typogrify import filters
import re
from bs4 import BeautifulSoup

from django.utils.deprecation import MiddlewareMixin


"""
Enhance typography by passing text though :
- typogrify
- our own french typographic rules
"""


def french_typography(text):
    """
    Add some rules for french typography
    Rules :
    - [sp],
    - [sp]:[sp]
    - [sp];[sp]
    - .[sp]
    - [sp]!
    - [sp]?
    """
    # import wdb; wdb.set_trace()
    tag_pattern = '</?\w+((\s+\w+(\s*=\s*(?:".*?"|\'.*?\'|[^\'">\s]+))?)+\s*|\s*)/?>'

    # Factoriser !
    comma_finder_1 = re.compile(r"(\S(?<!\d))(\s,\s|\s,|,\s|,)(\S)")
    comma_finder_2 = re.compile(r"(\S)(\s,\s|\s,|,\s|,)(\S(?<!\d))")
    dot_finder = re.compile(r"(!\.e\.s)(\S\D)(\s\.\s|\s\.|\.\s|\.)(\S\D|$)")
    doubledot_finder = re.compile(r"(\S)(\s:\s|\s:|:\s|:)(\S)")
    # dotcoma_finder = re.compile(r"(\S)(\s;\s|\s;|;\s|;)(\S)")
    question_finder = re.compile(r"(\S)(\s\?\s|\s\?|\?\s|\?)(\S|$)")
    exclamation_finder = re.compile(r"(\S)(\s\!\s|\s\!|\!\s|\!)(\S|$)")
    intra_tag_finder = re.compile(
        r'(?P<prefix>(%s)?)(?P<text>([^<]*))(?P<suffix>(%s)?)' % (
            tag_pattern,
            tag_pattern
        )
    )
    openingquote_finder = re.compile(r"(\S)(\s«\s|\s«|«\s|«)(\S)")
    closingquote_finder = re.compile(r"(\S)(\s»\s|\s»|»\s|»)(\S)")

    def recurse(child):
        if child.string is None:
            for oc in child.children:
                recurse(oc)
        else:
            text = child.string
            output = intra_tag_finder.sub(_comma_process, text)
            output = intra_tag_finder.sub(_dot_process, output)
            output = intra_tag_finder.sub(_doubledot_process, output)
            output = intra_tag_finder.sub(_question_process, output)
            output = intra_tag_finder.sub(_exclamation_process, output)
            output = intra_tag_finder.sub(_openingquote_process, output)
            output = intra_tag_finder.sub(_closingquote_process, output)
            # output = intra_tag_finder.sub(_dotcoma_process, output)

            if output != text:
                node_soup = BeautifulSoup(output, 'lxml')
                child.replace_with(node_soup)

        return
    
    def _comma_process(groups):
        prefix = groups.group('prefix') or ''
        text = comma_finder_1.sub(r"""\1, \3""", groups.group('text'))
        text = comma_finder_2.sub(r"""\1, \3""", groups.group('text'))
        suffix = groups.group('suffix') or ''
        return prefix + text + suffix

    def _dot_process(groups):
        prefix = groups.group('prefix') or ''
        text = dot_finder.sub(r"""\1. \3""", groups.group('text'))
        suffix = groups.group('suffix') or ''
        return prefix + text + suffix

    def _doubledot_process(groups):
        prefix = groups.group('prefix') or ''
        text = doubledot_finder.sub(
            r"""\1<span class="fine">&nbsp;</span>: \3""",
            groups.group('text')
        )
        suffix = groups.group('suffix') or ''
        return prefix + text + suffix

    def _question_process(groups):
        prefix = groups.group('prefix') or ''
        text = question_finder.sub(
            r"""\1<span class="fine">&nbsp;</span>? \3""",
            groups.group('text')
        )
        suffix = groups.group('suffix') or ''
        return prefix + text + suffix

    def _exclamation_process(groups):
        prefix = groups.group('prefix') or ''
        text = exclamation_finder.sub(
            r"""\1<span class="fine">&nbsp;</span>! \3""",
            groups.group('text')
        )
        suffix = groups.group('suffix') or ''
        return prefix + text + suffix

    def _openingquote_process(groups):
        prefix = groups.group('prefix') or ''
        text = openingquote_finder.sub(
            r"""\1 «<span class="fine">&nbsp;</span>\3""",
            groups.group('text')
        )
        suffix = groups.group('suffix') or ''
        return prefix + text + suffix

    def _closingquote_process(groups):
        prefix = groups.group('prefix') or ''
        text = closingquote_finder.sub(
            r"""\1<span class="fine">&nbsp;</span>» \3""",
            groups.group('text')
        )
        suffix = groups.group('suffix') or ''
        return prefix + text + suffix

    # def _dotcoma_process(groups):
    #     prefix = groups.group('prefix') or ''
    #     text = dotcoma_finder.sub(r"""\1<span class="fine">&nbsp;</span>; \3""", groups.group('text'))
    #     suffix = groups.group('suffix') or ''
    #     return prefix + text + suffix

    soup = BeautifulSoup(text, 'lxml')

    for item in soup.findAll('div'):
        for child in item.children:
            recurse(child)

    output = soup.prettify()
    output = output.replace('<html>', '').replace('<body>', '').replace('</html>', '').replace('</body>', '')
    return output


def myapplyfilters(text):
    """
    Redefine typogrify applyfilter
    in order to add our own
    """
    if len(text) > 100:
        text = filters.amp(text)
        text = filters.smartypants(text)
        # text = filters.caps(text)
        text = filters.initial_quotes(text)
        # text = french_typography(text)

    return text


class PrettyText(MiddlewareMixin):
    """
    Remove spaces between tags in HTML responses to save on bandwidth
    """
    # def __init__(self, get_response):
    #     self.get_response = get_response

    # def __call__(self, request):
    #     return self.get_response(request)

    def process_response(self, request, response):
        if 'text/html' in response.get('Content-Type', ''):
            rc = response.content.decode('utf-8')
            if 'data-json' in rc or 'data-colview' in rc or 'data-colinfo' in rc:
                return response

            filters.applyfilters = myapplyfilters
            text = filters.typogrify(
                response.content.decode('utf-8'),
                ignore_tags=[
                    'style', 'script', 'head', 'pre',
                    'code', 'input', 'textarea', 'option',
                    'picture', 'svg', 'noscript', 'a',
                    'fieldset',
                    'no_pretifier',
                    'no_prettifier',
                    'no_prettyfier',
                    '.no_prettyfier',
                ]
            )
            text = french_typography(text)
            response.content = text.encode('utf-8')
        return response
